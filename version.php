<?php 
/**
 * Version of BigBlueButtonBN Activity Module for Moodle 1.9
 *
 * Authors:
 *      Fred Dixon (ffdixon [at] blindsidenetworks [dt] org)
 *      Jesus Federico  (jesus [at] blindsidenetworks [dt] com)    
 *
 * @package   mod_bigbluebuttonbn
 * @copyright 2010-2012 Blindside Networks
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v2 or later
 */


$module->version  = 2012112706;  // The current module version (Date: YYYYMMDDXX)
$module->requires = 2007101509;  // The current module version (Date: YYYYMMDDXX)
$module->cron     = 0;           // Period for cron to check this module (secs)
$module->component = 'mod_bigbluebuttonbn';
$module->maturity = MATURITY_ALPHA;  // [MATURITY_STABLE | MATURITY_RC | MATURITY_BETA | MATURITY_ALPHA]
$module->release  = '1.0.8';
